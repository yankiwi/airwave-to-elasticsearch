import asyncio
import aiohttp
import datetime
import json
import logging
import os
import pytz
import re
import requests
import socket
import sys
import time
import xmltodict
import yaml

from asyncio import coroutine

# Get Config
try:
    main_conf_file = open(os.path.join(
        os.path.dirname(__file__), 'config', 'main.yaml'), 'r')
    main_conf = yaml.safe_load(main_conf_file)
    if main_conf['stage'].lower() == "production":
        stage_conf_file = open(os.path.join(
            os.path.dirname(__file__), 'config', 'prod.yaml'), 'r')
        stage_conf = yaml.safe_load(stage_conf_file)
    elif main_conf['stage'].lower() == "development":
        stage_conf_file = open(os.path.join(
            os.path.dirname(__file__), 'config', 'dev.yaml'), 'r')
        stage_conf = yaml.safe_load(stage_conf_file)
    else:
        print('ERROR - Unable to start, invalid application stage.')
        sys.exit(1)
except Exception as e:
    print(e)
    sys.exit(1)

# Setup logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
try:
    log_path = "{0}/{1}.log".format(stage_conf['logging']['path'], os.path.basename(__file__))
except Exception as e:
    print('WARNING - {}'.format(e))
    log_path = "{0}.log".format(os.path.basename(__file__))

# Logger Console Handler
ch = logging.StreamHandler(sys.stdout)
ch.propagate = False
ch.setLevel(stage_conf['logging']['console_level'])
ch_format = logging.Formatter('%(asctime)s - %(levelname)-8s - %(message)s')
ch.setFormatter(ch_format)
logger.addHandler(ch)

# Logger File Handler
fh = logging.FileHandler(log_path)
fh.propagate = False
fh.setLevel(stage_conf['logging']['file_level'])
fh_format = logging.Formatter('%(asctime)s - %(levelname)-8s - %(message)s')
fh.setFormatter(fh_format)
logger.addHandler(fh)

poll_time = datetime.datetime.now(
    pytz.timezone(main_conf['timezone'])).isoformat()
filter_time = int((time.time() - (60)) * 1000)

client_list = []
ctlr_ap_list = {}


def get_ap_list():

    start = time.time()

    requests.packages.urllib3.disable_warnings()
    session = requests.Session()
    url = (main_conf['airwave']['url'] + 'LOGIN')
    destination = '/'
    next_action = ''
    params = {'credential_0': main_conf['airwave']['username'],
              'credential_1': main_conf['airwave']['password'],
              'login': 'Log In',
              'destination': destination,
              'next_action': next_action}
    response = session.post(url, params=params, verify=False)

    if (response.status_code != 200):
        logger.debug('login failed')
        return False

    logger.info("Login took: {:.2f} seconds".format(time.time() - start))
    start = time.time()

    url = (main_conf['airwave']['url'] + 'ap_list.xml')

    response = session.get(url, verify=False)
    data = xmltodict.parse(response.text)

    logger.info("ap_list fetch took: {:.2f} seconds".format(time.time() - start))

    session.close()

    return data['amp:amp_ap_list']['ap']


@coroutine
async def fetch_ap_all(url_list, ap_list, ctlr_list):

    start = time.time()

    connector = aiohttp.TCPConnector(limit=main_conf['polling']['concurrency'], verify_ssl=False)
    url = (main_conf['airwave']['url'] + 'LOGIN')
    destination = '/'
    next_action = ''
    params = {'credential_0': main_conf['airwave']['username'],
              'credential_1': main_conf['airwave']['password'],
              'login': 'Log In',
              'destination': destination,
              'next_action': next_action}

    with aiohttp.ClientSession(connector=connector) as session:
        async with session.post(url, params=params) as response:
            if (response.status != 200):
                logger.debug('login failed')
                return False
            await asyncio.gather(
                *[fetch_ap_detail(session, url, index, ap_list, ctlr_list) for index, url in enumerate(url_list, start=0)],
                return_exceptions=True)

    await session.close()

    logger.info("fetch ap detail took: {:.2f} seconds".format(time.time() - start))


@coroutine
async def fetch_ap_detail(session, url, index, ap_list, ctlr_list):
    for attempt in range(main_conf['polling']['retries']):
        try:
            async with session.get(url) as response:
                data = xmltodict.parse(await response.text())

                msgDict = {}
                msgDict['ap'] = {}
                msgDict['ap_reachability'] = {}
                msgDict['ap_status'] = {}
                msgDict['client'] = {}

                count = 0
                b_count = 0
                a_count = 0

                msgDict['@metadata'] = {'type': 'ap_details'}
                msgDict['@timestamp'] = poll_time
                msgDict['system'] = 'aruba'

                if 'name' in ap_list[int(index)]:
                    msgDict['ap']['name'] = ap_list[int(index)]['name']
                    ap_name = re.search('^(.*)(ap|ids|test)[^d]+',
                                        ap_list[int(index)]['name'], re.IGNORECASE)
                    if ap_name:
                        ap_name_loc = ap_name.group(1)
                    else:
                        ap_name_loc = "UNKNOWN"
                    msgDict['ap']['nameLocation'] = ap_name_loc
                if 'firmware' in ap_list[int(index)]:
                    msgDict['ap']['softwareVersion'] = ap_list[int(index)]['firmware']
                if 'model' in ap_list[int(index)]:
                    msgDict['ap']['model'] = ap_list[int(index)]['model']['#text']
                if 'lan_mac' in ap_list[int(index)]:
                    msgDict['ap']['ethernetMac'] = ap_list[int(index)]['lan_mac']
                if 'lan_ip' in ap_list[int(index)]:
                    msgDict['ap']['ipAddress'] = ap_list[int(index)]['lan_ip']
                if 'syslocation' in ap_list[int(index)]:
                    msgDict['ap']['syslocation'] = ap_list[int(index)]['syslocation']
                if 'last_contacted' in ap_list[int(index)]:
                    if ap_list[int(index)]['last_contacted'] != 'never':
                        msgDict['ap']['serialNumber'] = ap_list[int(index)]['serial_number']
                    else:
                        msgDict['ap']['serialNumber'] = 'UNKNOWN'
                if 'is_up' in ap_list[int(index)]:
                    if ap_list[int(index)]['is_up'] == 'true':
                        ap_status = "UP"
                        ap_reachability = "REACHABLE"
                        msgDict['ap']['controllerName'] = ctlr_list[ap_list[int(index)]['controller_id']]
                        msgDict['ap']['upTime'] = ap_list[int(index)]['snmp_uptime']
                        ctlr_ap_list[msgDict['ap']['name']] = ctlr_list[ap_list[int(index)]['controller_id']]
                    else:
                        ap_status = "DOWN"
                        ap_reachability = "UNREACHABLE"
                        msgDict['ap']['controllerName'] = "UNKNOWN"
                        ctlr_ap_list[msgDict['ap']['name']] = "UNKNOWN"
                        msgDict['ap']['upTime'] = 0
                    msgDict['ap_status'] = {
                        'status': ap_status,
                        ap_status: 1}
                    msgDict['ap_reachability'] = {
                        'reachabilityStatus': ap_reachability,
                        ap_reachability: 1}
                if 'client_count' in ap_list[int(index)]:
                    count = ap_list[int(index)]['client_count']
                    if 'radio' in data['amp:amp_ap_detail']['ap']:
                        for radio in data['amp:amp_ap_detail']['ap']['radio']:
                            if 'client' in radio:
                                if isinstance(radio['client'], list):
                                    for sclient in radio['client']:
                                        if re.match('b.*', radio['radio_type']):
                                            b_count = (b_count + 1)
                                            client_list.append(sclient['radio_mac'])
                                        if re.match('a.*', radio['radio_type']):
                                            a_count = (a_count + 1)
                                            client_list.append(sclient['radio_mac'])
                                else:
                                    if re.match('b.*', radio['radio_type']):
                                        b_count = (b_count + 1)
                                        client_list.append(radio['client']['radio_mac'])
                                    if re.match('a.*', radio['radio_type']):
                                        a_count = (a_count + 1)
                                        client_list.append(radio['client']['radio_mac'])
                msgDict['client'] = {
                    'clientCount': int(count),
                    'clientCount_2_4GHz': int(b_count),
                    'clientCount_5GHz': int(a_count)}
                for send_attempt in range(main_conf['polling']['retries']):
                    try:
                        if main_conf['logstash']['ip'].lower() == 'tcp':
                            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        else:
                            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        sock.settimeout(main_conf['polling']['timeout'])
                        sock.connect((main_conf['logstash']['ip'], main_conf['logstash']['port']))
                        sock.settimeout(None)
                        msg = (json.dumps(msgDict) + '\n').encode('utf-8')
                        logger.debug(msg)
                        sock.sendall(msg)
                        sock.close()
                    except Exception as e:
                        logger.warning(e)
                        await asyncio.sleep(1)
                    else:
                        break
                else:
                    logger.error('giving up on socket send retries')
        except Exception as e:
            logger.warning(e)
            await asyncio.sleep(1)
        else:
            break
    else:
        logger.error('giving up on polling retries')
    return True


@coroutine
async def fetch_client_all(url_list):

    start = time.time()

    connector = aiohttp.TCPConnector(limit=main_conf['polling']['concurrency'], verify_ssl=False)
    url = (main_conf['airwave']['url'] + 'LOGIN')
    destination = '/'
    next_action = ''
    params = {'credential_0': main_conf['airwave']['username'],
              'credential_1': main_conf['airwave']['password'],
              'login': 'Log In',
              'destination': destination,
              'next_action': next_action}

    with aiohttp.ClientSession(connector=connector) as session:
        async with session.post(url, params=params) as response:
            if (response.status != 200):
                logger.debug('login failed')
                return False
            await asyncio.gather(
                *[fetch_client_detail(session, url, index) for index, url in enumerate(url_list, start=0)],
                return_exceptions=True)

    await session.close()

    logger.info("fetch client detail took: {:.2f} seconds".format(time.time() - start))


@coroutine
async def fetch_client_detail(session, url, index):
    for attempt in range(main_conf['polling']['retries']):
        try:
            async with session.get(url) as response:
                data = xmltodict.parse(await response.text())

                msgDict = {}
                msgDict['controller'] = {}
                msgDict['ap'] = {}
                msgDict['client'] = {}

                msgDict['@metadata'] = {'type': 'client_details'}
                msgDict['@timestamp'] = poll_time
                msgDict['system'] = 'aruba'

                if '@mac' in data['amp:amp_client_detail']['client']:
                    msgDict['client']['macAddress'] = str(data['amp:amp_client_detail']['client']['@mac']).lower()
                if 'username' in data['amp:amp_client_detail']['client']:
                    msgDict['client']['userName'] = data['amp:amp_client_detail']['client']['username']
                if 'ap' in data['amp:amp_client_detail']['client']:
                    msgDict['ap']['name'] = data['amp:amp_client_detail']['client']['ap']['#text']
                if 'auth_stat' in data['amp:amp_client_detail']['client']:
                    msgDict['client']['authenticated'] = data['amp:amp_client_detail']['client']['auth_stat']
                if 'assoc_stat' in data['amp:amp_client_detail']['client']:
                    msgDict['client']['associated'] = data['amp:amp_client_detail']['client']['assoc_stat']
                if 'auth_type' in data['amp:amp_client_detail']['client']:
                    auth_type = re.search(r'^(.*)\s+\((.*)\)',
                                          data['amp:amp_client_detail']['client']['auth_type'],
                                          re.IGNORECASE)
                    if auth_type:
                        securityPolicy = auth_type.group(1)
                        eapType = auth_type.group(2)
                    else:
                        securityPolicy = 'UNKNOWN'
                        eapType = 'UNKNOWN'
                    msgDict['client']['securityPolicy'] = securityPolicy
                    msgDict['client']['eapType'] = eapType
                if 'device_type' in data['amp:amp_client_detail']['client']:
                    msgDict['client']['device_type'] = data['amp:amp_client_detail']['client']['device_type']
                if 'radio_mode' in data['amp:amp_client_detail']['client']:
                    msgDict['client']['protocol'] = ("DOT11" + data['amp:amp_client_detail']['client']['radio_mode'])
                if 'signal' in data['amp:amp_client_detail']['client']:
                    msgDict['client']['rssi'] = int(data['amp:amp_client_detail']['client']['signal'])
                if 'snr' in data['amp:amp_client_detail']['client']:
                    msgDict['client']['snr'] = int(data['amp:amp_client_detail']['client']['snr'])
                if 'ssid' in data['amp:amp_client_detail']['client']:
                    msgDict['client']['ssid'] = data['amp:amp_client_detail']['client']['ssid']
                if 'vlan' in data['amp:amp_client_detail']['client']:
                    msgDict['controller']['vlan'] = data['amp:amp_client_detail']['client']['vlan']
                if data['amp:amp_client_detail']['client']['ap']['#text'] in ctlr_ap_list:
                    msgDict['controller']['deviceName'] = ctlr_ap_list[data['amp:amp_client_detail']['client']['ap']['#text']]
                else:
                    msgDict['controller']['deviceName'] = 'UNKNOWN'
                msgDict['client']['count'] = 1
                for send_attempt in range(main_conf['polling']['retries']):
                    try:
                        if main_conf['logstash']['ip'].lower() == 'tcp':
                            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        else:
                            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        sock.settimeout(main_conf['polling']['timeout'])
                        sock.connect((main_conf['logstash']['ip'], main_conf['logstash']['port']))
                        sock.settimeout(None)
                        msg = (json.dumps(msgDict) + '\n').encode('utf-8')
                        logger.debug(msg)
                        sock.sendall(msg)
                        sock.close()
                    except Exception as e:
                        logger.warning('problem in send_attempt')
                        logger.warning(e)
                        await asyncio.sleep(1)
                    else:
                        break
                else:
                    logger.error('giving up on socket send retries')
        except Exception as e:
            logger.warning('problem in poll attempt')
            logger.warning(e)
            logger.debug(data)
            await asyncio.sleep(1)
        else:
            break
    else:
        logger.error('giving up polling on retries')
    return True


if __name__ == '__main__':

    ap_list_full = get_ap_list()

    if ap_list_full is not False:
        ap_list = []
        url_list = []
        ctlr_list = {}

        for ap in ap_list_full:
            if ap['device_category'] == 'thin_ap':
                ap_list.append(ap)
                url_list.append(main_conf['airwave']['url'] + 'ap_detail.xml?id=' + ap['@id'])
            if ap['device_category'] == 'controller':
                ctlr_list = {**ctlr_list, ap['@id']: ap['name']}

        loop = asyncio.get_event_loop()
        loop.run_until_complete(fetch_ap_all(url_list, ap_list, ctlr_list))

    if client_list:
        for client in client_list:
            url_list.append(main_conf['airwave']['url'] + 'client_detail.xml?mac=' + client + '&limit=1')

        loop2 = asyncio.get_event_loop()
        loop2.run_until_complete(fetch_client_all(url_list))
