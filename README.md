# AirWave to ElasticSearch

Python scripts to read Aruba AirWave API, parse data and export to ElasticSearch via Logstash.